package com.movielist_springboot.ksh980212.service;

import com.movielist_springboot.ksh980212.domain.user.User;
import com.movielist_springboot.ksh980212.domain.user.UserRepository;
import com.movielist_springboot.ksh980212.web.dto.RequestUserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    @Transactional
    public User save(RequestUserDto requestUserDto){
        return userRepository.save(requestUserDto.toEntity());
    }

    @Transactional
    public List<User> findAll(){
        return userRepository.findAll();
    }

    @Transactional
    public Optional<User> findById(Long id){
        return userRepository.findById(id);
    }

    @Transactional
    public Optional<User> findByName(String name){
        return userRepository.findByName(name);
    }
}
