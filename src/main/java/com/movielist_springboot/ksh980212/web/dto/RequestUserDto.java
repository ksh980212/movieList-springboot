package com.movielist_springboot.ksh980212.web.dto;

import com.movielist_springboot.ksh980212.domain.user.User;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class RequestUserDto {

    private String name;
    private int age;
    private String movies;

    public RequestUserDto(String name, int age, String movies){
        this.name = name;
        this.age = age;
        this.movies = movies;
    }

    @Builder
    public User toEntity(){
        return User.builder().
                name(name)
                .age(age)
                .movies(movies)
                .build();
    }
}
