package com.movielist_springboot.ksh980212.web.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserSaveResponseDto {
    private final String message;

}
