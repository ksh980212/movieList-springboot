package com.movielist_springboot.ksh980212.web;

import com.movielist_springboot.ksh980212.domain.user.User;
import com.movielist_springboot.ksh980212.service.UserService;
import com.movielist_springboot.ksh980212.web.dto.RequestUserDto;
import com.movielist_springboot.ksh980212.web.dto.UserSaveResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class UserApiController {

    private final UserService userService;

    @PostMapping("/api/v1/user")
    public UserSaveResponseDto userSave(@RequestBody RequestUserDto requestUserDto){
        userService.save(requestUserDto);
        return new UserSaveResponseDto("User save Ok");
    }

    @GetMapping("/api/v1/user")
    public List<User> userLoad(){
        return userService.findAll();
    }

    @GetMapping("/api/v1/user/id/{id}")
    public Optional<User> userFindById(@PathVariable Long id){
        return userService.findById(id);
    }
    @GetMapping("/api/v1/user/name/{name}")
    public Optional<User> userFindByName(@PathVariable String name){
        return userService.findByName(name);
    }
}
