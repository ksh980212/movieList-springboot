package com.movielist_springboot.ksh980212.domain.user;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length=20, nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer age;

    @Column(length= 50, nullable = false)
    private String movies;

    @Builder
    public User(String name, int age, String movies){
        this.name = name;
        this.age = age;
        this.movies = movies;
    }
}
