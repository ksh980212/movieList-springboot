package com.movielist_springboot.ksh980212.domain.user;

import com.movielist_springboot.ksh980212.web.dto.RequestUserDto;
import org.junit.Test;
import org.junit.runner.Request;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired  // Bean 생성
    private UserRepository userRepository;

    @Test
    public void User가_저장된다(){
        //given
        String name = "강승호";
        int age= 22;
        String movies = "광해";

        userRepository.save(User.builder()
                .name(name)
                .age(age)
                .movies(movies)
                .build());

        //when
        List<User> userList = userRepository.findAll();

        //then
        User user = userList.get(0);
        assertThat(user.getName()).isEqualTo(name);
        assertThat(user.getAge()).isEqualTo(age);
        assertThat(user.getMovies()).isEqualTo(movies);
    }

    @Test
    public void 모든_유저정보가_불러와진다(){
        //given
        userRepository.save(User.builder()
                .name("name")
                .age(20)
                .movies("movies")
                .build());
        userRepository.save(User.builder()
                .name("name2")
                .age(22)
                .movies("movies2")
                .build());

        //when
        List<User> userList = userRepository.findAll();
        assertThat(userList.size()).isEqualTo(2);
    }

    @Test
    public void 아이디검색_기능_정상적이다(){
        //given
        userRepository.save(User.builder()
                .name("name")
                .age(20)
                .movies("movies")
                .build());

        //when
        Optional<User> user = userRepository.findById(1L);

        //then
        assertThat(user).isNotNull();

    }

    @Test
    public void 이름검색_기능_정상적이다(){
        //given
        userRepository.save(User.builder()
                .name("name")
                .age(20)
                .movies("movies")
                .build());

        //when
        Optional<User> user = userRepository.findByName("name");

        //then
        assertThat(user).isNotNull();
    }
}
